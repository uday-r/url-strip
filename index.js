module.exports.getCompanyName = function(url) {
  var pattern = /^(?:https?:\/\/)?([^@\/\n]+@)?([^:?\/\n]+)/g;
  var domain =url.match(pattern)[0];
  domain = domain.replace(/\.(com|org|co\.uk|in)$/, '')
  var domainArray = domain.split('.');
  return domainArray[domainArray.length - 1];
}
