var index = require('../index.js');

describe('Get company name', function() {
  it('should return google', function() {
    var companyName = index.getCompanyName('http://www.google.com');
    expect(companyName).toBe('google');
  });

  it('should return facebook', function() {
    var companyName = index.getCompanyName('http://www.facebook.com');
    expect(companyName).toBe('facebook');
  });

  it('should return google from subdomain', function() {
    var companyName = index.getCompanyName('http://mail.google.com');
    expect(companyName).toBe('google');
  });

  it('should return google from long subdomain', function() {
    var companyName = index.getCompanyName('http://mail.com.google.com');
    expect(companyName).toBe('google');
  });

  it('should return google from URL without protocol', function() {
    var companyName = index.getCompanyName('mail.com.google.com');
    expect(companyName).toBe('google');
  });

  it('should return google from co.uk', function() {
    var companyName = index.getCompanyName('http://mail.com.google.co.uk');
    expect(companyName).toBe('google');
  });

  it('should return google which has no slash', function() {
    var companyName = index.getCompanyName('mail.com.google.com?q=www.fb.com');
    expect(companyName).toBe('google');
  });
});
